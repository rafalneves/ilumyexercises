var users = require("./users.js");

var async = require('async');
var nodemailer = require('nodemailer');
var transporter = nodemailer.createTransport();

async.eachSeries(users, function (user, cb) {
    async.auto({
        //Construct the object for sending email
        buildEmail: function (cb, r) {
            cb(null, {
                to: user.email,
                from: 'info@ilumy.com',
                subject: 'testmail',
                text: 'testmail'
            });
        },
        //Send the email
        sendMail: ['buildEmail', function (cb, r) {
            if (r.buildEmail.to) {
                console.log('sending mail', r.buildEmail.to);
                transporter.sendMail(r.buildEmail, cb);
            } else
                cb();
        }]
    }, function (err, results) {
       //Treat error if you want > [else] keepgoing
        if (err)
            console.log(err);

        cb();
    });
}, function (err, results) {
    console.log("All users imported")
})