# Ilumy Exercises #

## Part 1: Problem solving ##
Resolve the bugs in the following code

```
#!javascript

package.json
{
 "name": "ILUMYsample",
 "version": "1.0.0",
 "description": "Code sample",
 "main": "app.js",
 "dependencies": {
   "async": "^1.5.0",
   "nodemailer": "^1.10.0"
 }
}
```


users.js

```
#!javascript

module.exports = [
   { name: 'Bert', age: 30, email:'bert@ilumy.com', title: 'Developer', postNumber: 3439 },
   { name: 'Ruben de Wit', age: 21, email:'ruben@ilumy.com', title: 'Developer', postNumber: 213 },
   { name: 'Bart', age: 33, email:'bart@ilumy.com', title: 'Developer', postNumber: 3439 },
   { name: 'Harry', age: 30, email:'harry@ilumy.com', title: 'Developer', postNumber: 3439 },
   { name: 'Rudolf van den Oranjestraat', age: 30, title: 'Head of Developemt', postNumber: 3439 },
   { name: 'Lucas', age: 24, email:'lucas@ilumy.com', title: 'Developer', postNumber: 3439 },
   { name: 'Marie', age: 55, email:'marie@ilumy.com', title: 'Developer', postNumber: 3439 },
   { name: 'Brigit', age: 33, email:'brigit@ilumy.com', title: 'Developer', postNumber: 3439 },
   { name: 'Lisa', age: 21, email:'lisa@ilumy.com', title: 'Developer', postNumber: 3439 },
   { name: 'Linda', age: 33, email:'linda@ilumy.com', title: 'Developer', postNumber: 3439 }
]
```


app.js

```
#!javascript

var users = require("./users.js");

var async = require('async');
var nodemailer = require('nodemailer');
var transporter = nodemailer.createTransport();

async.eachSeries(users, function(user, nextUser) {
   async.auto({
       //Construct the object for sending email
       createMail: function(cb, r) {
           cb(null, {
               to: user.email,
               from: 'info@ilumy.com',
               subject: 'testmail',
               text: 'testmail'
           });
       },
       //Send the email
       sendMail: ['buildEmail', function(cb, r) {
           if(r.buildEmail.email) {
               console.log('sending mail', r.buildMail.email);
               transporter.sendMail(r.buildMail, cb);
           }
       }]
   }, nextUser);
}, function(err, results) {
   console.log("All users imported")
})

```

### Part 2: Show your skills ###
```
#!javascript
Build a meeting web application using the following technologies:
- Node.js
- MongoDb

BONUS:
- Setup the frontend in AngularJs

The database contains:
- Start and end date/time of a meeting
- Topic of the meeting
- Names of people in the meeting (max 10)

The App shows the following
- Button to insert 5.000 additional meetings with a given topic at random dates with random durations (max 8 hours) and random members out of a list of 10
- Show the total amount of meetings in the database
- Show the upcoming 5 meetings starting from today, topic, people, date / time
- Show the average amount of people in the next 20 meetings
- Show the next meeting date of each person

Note: If you have difficulties implementing this in Mongodb, you can use Mysql, but I prefer that you show me how quickly you adapt new technologies.
```