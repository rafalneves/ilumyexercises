var app = angular.module("part2", ["ngRoute", "ngProgress", "ui.bootstrap"]);
app.config(["$routeProvider", function($routeProvider){
	$routeProvider
	.when("/", {
		templateUrl: "/views/index/main.html",
		controller: "mainController"
	})
	.otherwise({
		redirectTo: "/"
	});
}]);
app.controller("mainController", function($scope, $http, ngProgress, $q, $window, $route, $uibModal){
	$scope.generate = function(){
		$uibModal.open({
			templateUrl: "generate.html",
			controller: "generateController",
			size: "md",
			resolve: {
				callback: function(){
					return callback;
				}
			}
		});
		function callback(){
			getData();
		}
	}
	
	function getData(){
		ngProgress.start();
		var deferred = $q.defer();
		var calls = new Array();

		calls.push($http.get("/numberof/meetings.plain").success(function(data){
			$scope.numberOf = data;
		}));
		calls.push($http.get("/upcoming/meetings/5.json").success(function(data){
			$scope.upcoming = data;
		}));
		calls.push($http.get("/upcoming/meetings/20/avg/people.plain").success(function(data){
			$scope.avg20 = data;
		}));
		calls.push($http.get("/meeting/next/by/person.json").success(function(data){
			$scope.nextByPeople = data;
		}));
		$q.all(calls).then(function(){
			ngProgress.complete();
		});

	}
	getData();
});
app.controller("generateController", function($scope, $http, ngProgress, $uibModalInstance, callback){
	$scope.loopError = function(form, p){
		for(var i = 0; i < p.length; i++)
			if(form['email' + i].$invalid)
				return true;
		return false;
	}
	
	$scope.f = {
		people: [{}]
	};
	
	$scope.addPeople = function(f){
		if(f.people.length < 10) f.people.push({});
	}
	
	$scope.submitted = false;
	$scope.submitting = false;
	$scope.submit = function(form, data){
		$scope.submitted = true;

		if(form.$invalid)
			return;

		$scope.submitting = true;

		ngProgress.start();
		$http({
			url: "/generate.json",
			dataType: "application/json",
			method: "post",
			headers: {
				"Content-Type": "application/json"
			},
			data: data
		}).
		success(function(data, status, headers, config){
			ngProgress.complete();

			callback();

			$uibModalInstance.dismiss("cancel");
		}).
		error(function(data, status, headers, config){});
	}

	$scope.close = function(event){
		$uibModalInstance.dismiss(event);
	};
});