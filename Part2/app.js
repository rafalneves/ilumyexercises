var express = require("express");
var fs = require("fs");

var mongoose = require("mongoose"), Schema = mongoose.Schema;
mongoose.connect("mongodb://192.168.32.99/part2"); //Conf options should came from conf file or somehting like that.
var db = mongoose.connection;
db.on("error", console.error.bind(console, "connection error:"));
db.once("open", function(){ console.log("ready to go") });
	
var async = require("async");
var bodyParser = require("body-parser");

var meetingSchema = mongoose.Schema({
	id: Schema.Types.ObjectId,
	topic: String,
	datetime: Date,
	duration: Number,
	people: Array
});
var Meeting = mongoose.model("Meeting", meetingSchema);

var app = express();
app.use("/public", express.static("public"));
app.use("/views", express.static("views"));
app.use(bodyParser.json({ limit: "150mb" }));

app.get('/', function(request, response){
	fs.readFile(__dirname + "/views/index/index.html", function(error, data){
		response.writeHead(200, { "Content-Type": "text/html" });
		response.end(data);
	});
});
app.get("/upcoming/meetings/:number.:format(json)", function(request, response){
	Meeting.find({"datetime": {$gt: new Date() } }).sort({"datetime": 1 }).limit(Number(request.params.number)).exec(function(error, data){
		if(error){
			response.writeHead(403, { "Content-Type": "text/plain" });
			response.end("SomethingWrong");
		}else{
			response.writeHead(200, { "Content-Type": "application/json" });
			response.end(new Buffer(JSON.stringify(data), "UTF8"));
		}
	});
});
app.get("/numberof/meetings.:format(plain)", function(request, response){
	Meeting.count({}, function(error, count) {
		if(error){
			response.writeHead(403, { "Content-Type": "text/plain" });
			response.end("SomethingWrong");
		}else{
			response.writeHead(200, { "Content-Type": "text/plain" });
			response.end(String(count || 0));
		}
	});
});
app.get("/upcoming/meetings/:number/avg/people.:format(plain)", function(request, response){
	Meeting.aggregate([
        {
			$match: {"datetime": {$gt: new Date() } }
		},
		{
			$project:
			{
				count: {$size: "$people"}
			}
		},
		{
			$group: {
				_id: "$id",
				avg: { $avg: "$count" }
			}
		}
    ]).exec(function(error, data){
		if(error){
			response.writeHead(403, { "Content-Type": "text/plain" });
			response.end("SomethingWrong");
		}else{
			response.writeHead(200, { "Content-Type": "text/plain" });
			response.end(String(data.length ? data[0].avg || 0 : 0));
		}
	});
});
app.get("/meeting/next/by/person.:format(json)", function(request, response){
	Meeting.aggregate([
		{
			$unwind: "$people"
		},
		{
			$group: {
				_id: "$people"
			}
		}
    ]).exec(function(error, data){
		if(error)
			console.log(error);
		//I will ignore errors here...
			
		var asyncTasks = new Array();
		data.forEach(function(person, index){
			asyncTasks.push(function(callback){
				Meeting.find({"datetime": {$gt: new Date() }, "people.email": person._id.email }).sort({"datetime": 1 }).limit(1).exec(function(error, data){
					if(error)
						callback(error);
					else{
						person.nextMeeting = data.length ? data[0] : null;
						
						callback();
					}
				});
			});
		});
		async.parallel(asyncTasks, function(error){
			if(error){
				response.writeHead(403, { "Content-Type": "text/plain" });
				response.end("SomethingWrong");
			}else{
				response.writeHead(200, { "Content-Type": "application/json" });
				response.end(new Buffer(JSON.stringify(data), "UTF8"));
			}
		});
	});
});

app.post("/generate.:format(json)", function(request, response){
	/*Meeting.remove({}, function(error){
		if(error)
			console.log(error);
		//I will ignore errors here...
	*/
		var asyncTasks = new Array(), start = new Date("2016-01-01"), end = new Date("2017-01-01");
		for(var i = 0; i < 5000; i++){
			asyncTasks.push(function(callback){
				var m = new Meeting({
					topic: request.body.topic,
					datetime: new Date(start.getTime() + Math.random() * (end.getTime() - start.getTime())),
					duration: Math.floor(Math.random() * 8) + 1 ,
					people: getRandomPeople()
				});
				m.save(function(error){
					callback(error);
				});
			});
		}
		async.parallel(asyncTasks, function(error){
			if(error)
				console.log(error);
			//I will ignore errors here...
			
			response.writeHead(200, { "Content-Type": "text/plain" });
			response.end();
		});
	//});
	
	function getRandomPeople(){
		var lp = new Array(), n = Math.floor(Math.random() * request.body.people.length) + 1;

		while(lp.length < n){
			var p = request.body.people[Math.floor(Math.random() * request.body.people.length)];
			
			var there = false;
			for(var y = 0; y < lp.length; y++)
				if(lp[y].email == p.email){
					there = true;
					break;
				}
			
			if(!there)
				lp.push(p);
		}
		return lp;
	}
});

app.listen(3000);